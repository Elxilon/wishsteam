# TODOLIST

## SERVER :
- [X] Get all games from a user wishlist
- [X] Get a random game from ~~a user~~ wishlist
- [X] Get a game (all his information)
- [X] Cookie with user ID
- [ ] Get all games from user games (?)

## CLIENT :
- [ ] Input to define the user ID (default : elxilon)
- [ ] Homepage random game show
- [ ] Page with the full list of games from the user wishlist
- [ ] Add filters :
  - [ ] Name
  - [ ] Price
  - [ ] Categories
  - [ ] Genres
  - [ ] Release date
  - [ ] Number of recommendations
- [ ] Compare wishlist with friends :
  - [ ] Wishlist
  - [ ] Games they already owned (?)