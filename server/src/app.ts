import express from 'express';
import { Router } from 'express';
import userRoutes from './routes/user';
import wishlistRoutes from './routes/wishlist';
import gamelistRoutes from './routes/game';

export function createApp(app: express.Application): void {
    const router = Router();

    router.use('/user', userRoutes);
    router.use('/wishlist', wishlistRoutes);
    router.use('/game', gamelistRoutes);

    app.use('/api', router);
}