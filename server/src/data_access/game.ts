import axios from "axios";

/**
 * GET game from game ID
 */
export const getGameFromId = async (gameId: string) => {
    const {data} = await axios.get(
        `https://store.steampowered.com/api/appdetails?appids=${gameId}`
    );
    return data[gameId];
}