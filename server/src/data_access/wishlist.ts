import axios from "axios";

/**
 * GET all games IDs from user's wishlist
 * userId can be the default or the custom id
 */
export const getAllGameIdFromUserIdWishlist = async (userId: string) => {
    const urlStr = /^\d{17}$/.test(userId) ? "profiles" : "id";

    const allGamesId = [];

    let iteration = 0;
    let respLen = 0;
    do {
        const wishlistUrl =
            `https://store.steampowered.com/wishlist/${urlStr}/${userId}/wishlistdata/?p=${iteration}`;
        const {data} = await axios.get(wishlistUrl);
        respLen = Object.keys(data).length;
        allGamesId.push(...Object.keys(data));
        iteration++;
    } while (respLen > 0)

    return allGamesId;
}