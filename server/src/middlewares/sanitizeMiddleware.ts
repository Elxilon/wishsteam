import {Request, Response, NextFunction} from "express";
import sanitizeHtml from 'sanitize-html';

/**
 * Clean all data from any form
 */
export const sanitizeFormData = (req: Request, _res: Response, next: NextFunction) => {
    req.body.userId = sanitizeHtml(req.body.userId);
    next();
};