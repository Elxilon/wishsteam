import { body } from 'express-validator';

export const validateFormData = [
    body('userId')
        .trim()
        .notEmpty()
        .withMessage('User ID is required')
        .isString()
        .withMessage('User ID must be a string of characters'),
];