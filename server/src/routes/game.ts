import { Router } from 'express';
import {getGameById, getRandomGameFromWishlist} from "../services/game";
const router: Router = Router();

/**
 * @swagger
 * /api/game/{gameId}:
 *   get:
 *     summary: Get a game by ID
 *     description:
 *     tags:
 *       - Game
 *     parameters:
 *       - in: path
 *         name: gameId
 *         description:
 *         required: true
 *         schema:
 *           type: string
 *           example: 382310
 *     responses:
 *       200:
 *         description: Successfully return a game by his ID.
 *       400:
 *         description: Game not found.
 *       500:
 *         description: Server error.
 */
router.get("/:gameId", getGameById);

/**
 * @swagger
 * /api/game/random:
 *   get:
 *     summary: Get a random game in the user's wishlist
 *     description:
 *     tags:
 *       - Game
 *     responses:
 *       200:
 *         description: Successfully return a random game from user wishlist.
 *       400:
 *         description: User not found.
 *       500:
 *         description: Server error.
 */
router.get("/random", getRandomGameFromWishlist);

export default router;