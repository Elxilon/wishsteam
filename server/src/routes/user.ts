import express, { Router } from 'express';
import {setUserId} from "../services/user";
import cookieParser from 'cookie-parser';
import {validateFormData} from "../middlewares/validationMiddleware";
import {sanitizeFormData} from "../middlewares/sanitizeMiddleware";
const router: Router = Router();

/**
 * @swagger
 * /api/user/set-user-id:
 *   post:
 *     summary: Define a cookie for the user ID
 *     description:
 *     tags:
 *       - User
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               userId:
 *                 type: string
 *     responses:
 *       200:
 *         description: Cookie define successfully.
 *       400:
 *         description: Missing user ID.
 */
router.post("/set-user-id",
    cookieParser(),
    express.json(),
    validateFormData, // Middleware data validation
    sanitizeFormData, // Middleware data cleaning
    setUserId
);

export default router;