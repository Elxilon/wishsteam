import { Router } from 'express';
import {getAllGameIdFromWishlist} from "../services/wishlist";
const router: Router = Router();

/**
 * @swagger
 * /api/wishlist/{userId}:
 *   get:
 *     summary: Get all game IDs from the user's wishlist
 *     description:
 *     tags:
 *       - Wishlist
 *     parameters:
 *       - in: path
 *         name: userId
 *         description:
 *         required: true
 *         schema:
 *           type: string
 *           example: elxilon
 *     responses:
 *       200:
 *         description: Successfully find all game IDs from user's wishlist.
 *       400:
 *         description: User not found.
 *       500:
 *         description: Server error.
 */
router.get("/:userId", getAllGameIdFromWishlist);

export default router;