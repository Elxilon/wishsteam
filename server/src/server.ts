import express from 'express';
import swaggerUi from 'swagger-ui-express';
import swaggerSpec from './swagger';
import { createApp } from './app';

const app = express();
const PORT = process.env.PORT || 3000;

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

createApp(app);

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});