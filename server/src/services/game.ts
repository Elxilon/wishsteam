import {Request, Response} from "express";
import {getAllGameIdFromUserIdWishlist} from "../data_access/wishlist";
import {getGameFromId} from "../data_access/game";
import crypto from 'crypto';

/**
 * GET a game by ID
 */
export const getGameById = async (req: Request, res: Response) => {
    const gameId = req.params.gameId;

    try {
        const game = await getGameFromId(gameId);
        res.status(200).json(game);
    }
    catch (error) {
        console.error("Error from GET random game from user's wishlist :", error);
        res.status(500).send('Error when trying to find game.');
    }
}

/**
 * GET a random game from user store in a cookie
 * if no user ID store in a cookie, 'elxilon' is defined by default
 */
export const getRandomGameFromWishlist = async (req: Request, res: Response) => {
    const userId = req.cookies.userId || 'elxilon';

    try {
        const allGamesId = await getAllGameIdFromUserIdWishlist(userId);

        const randomGameId = allGamesId[crypto.randomInt(0, allGamesId.length)];
        const randomGame = await getGameFromId(randomGameId);

        res.status(200).json(randomGame);
    }
    catch (error) {
        console.error("Error from GET random game from user's wishlist :", error);
        res.status(500).send('Error when trying to find a random game.');
    }
}