import {Request, Response} from "express";
import {validationResult} from "express-validator";

/**
 * POST the user ID and store it in a cookie
 */
export const setUserId = async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
        return res.status(400).json({ errors: errors.array() });

    res.cookie('userId', req.body.userId, { maxAge: 900000, httpOnly: true });
    res.send('Cookie define successfully.');
}