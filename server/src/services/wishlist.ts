import { Request, Response } from "express";
import {getAllGameIdFromUserIdWishlist} from "../data_access/wishlist";

/**
 * GET all games IDs from user's wishlist
 * userId can be the default or the custom id
 */
export const getAllGameIdFromWishlist = async (req: Request, res: Response) => {
    const userId = req.params.userId;

    try {
        const allGamesId = await getAllGameIdFromUserIdWishlist(userId);
        res.status(200).json(allGamesId);
    } catch (error) {
        console.error("Error from GET all games IDs from user's wishlist :", error);
        res.status(500).send(`Error when trying to find the wishlist of ${userId}`);
    }
}