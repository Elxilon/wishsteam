import swaggerJSDoc, { Options } from 'swagger-jsdoc';

const options: Options = {
    swaggerDefinition: {
        openapi: '3.0.0',
        info: {
            title: 'API Documentation for Bruce Wishlist',
            version: '1.0.0',
            description: "Quickview of all routes from the Bruce Wishlist's API.",
        },
    },
    apis: ['./src/routes/*.ts'],
};

const swaggerSpec = swaggerJSDoc(options);

export default swaggerSpec;
